/*
* Authors: Joseph Prause, Ashton Ansag, Simon Mcglynn
* Course: COP 4600 Fall
* Facility: UCF
*/
import java.io.*;
import java.util.*;
import static java.lang.System.out;

// A process has a Name an arrival time and a burst time (how long it takes)
class Process{
	private String name;
	private int arrival, burst, processNumber, firstBurst, firstArrival;
	public int timeRan = 0, timeIdle = 0;
	
	public Process( String name, int arrival, int burst, int processNumber ){
		this.name    = new String ( ""+ name );
		this.arrival = arrival;
		this.burst   = burst; 
		this.processNumber = processNumber;
		this.firstBurst = burst;
		this.firstArrival=arrival;
	}
	//getters and setters
	public String getName() {return name;}
	public void setName(String name) { this.name = name; }
	public int getArrival() { return arrival; }
	public void setArrival(int arrival) { this.arrival = arrival; }
	public int getProcessNumber() { return processNumber; }
	public void setProcessNumber(int processNumber) { this.processNumber = processNumber; }
	public int getBurst() { return burst; }
	public void setBurst(int burst) { this.burst = burst; }
	public int getFirstBurst() { return firstBurst; }
	public int getFirstArrival() { return firstArrival; }
}

// A scheduler has a processCount, a runfor time, a quantum time, and an alg  
public class Scheduler {

	// Number of processes, time schedule will run, time each process gets in rr	
	private static int processCount, runfor, quantum;

	// runtime, currentBurst, currentArrival time, standard counter
	private static int timeRun=0, cBurst=0, cArrival=0, i=0;

	// these variables will help with readablilty
	private static String cName = "";
	private static Boolean isRunning = true;
	private static Process current = null;

	/* The alg can be: fcfs, sjf, or rr*/
	private static String alg;
	
	// An arraylist to store all the processes 
	private static ArrayList<Process> list;

	// list the wait and turn times
	public static ArrayList<String> waitround = new ArrayList<String>();

	// checks when it arrived First only, used Hash duplicate check
	public static HashSet<Process> hasArrived = new HashSet<Process>();

	public static void main(String[] args){
		
		list = new ArrayList<Process>();
                alg = new String();
		try{ 
			read();
			System.setOut( new PrintStream( new File( "processes.out" ) ) );
 		}catch( FileNotFoundException ex ){}

		int ls = list.size();		
		out.println( ls +" Process"+ new String((ls==1) ? "":"es"));

		switch( alg ){ // algorithm used:
			case "fcfs": // first come first served
				out.println( "Using First Come First Served\n" );
				fcfs();
				break;
			case "sjf":  // shortest job first
				out.println( "Using Shortest Job First\n" );
				sjf();
				break;
			case "rr":   // round robin
				out.println( "Using Round Robin" );
				out.println( "Quantum "+ quantum +"\n" );
				rr();
				break;
		}
	}
	public static void read() throws FileNotFoundException{

		Scanner in = new Scanner( new File( "processes.in" ) );

		String str = in.next();
		i = 0;

		while( !str.equals( "process" ) ){
			
			if( str.equals( "processcount" ) ) processCount = in.nextInt();
			else if( str.equals( "runfor"  ) ) runfor = in.nextInt();
			else if( str.equals( "quantum" ) ) quantum = in.nextInt();
			else if( str.equals( "use"     ) ) alg = in.next();
			
			in.nextLine();
			str = in.next();
		}
		
		do{
			in.next(); // get "name"
			String name = in.next();
			in.next(); // get "arrival"
			int arrival = in.nextInt();
			in.next(); // get "burst"
			addInOrder( new Process( name, arrival, in.nextInt(), i) );
			
		} while( ++i<processCount && !in.next().equals( "end" ) );

		in.close();
	}

	// first come first served
	public static void fcfs(){

		// while there are processes or quantum to run through
		while( isRunning ){

			// we are at the not done get a ready process and carry on 
			if( list.size() != 0 ){
			
				// if the process is not ready to run, try the next one
				for( i=0; i<=list.size(); i++ ){
					if( list.get(i).getArrival() > timeRun ) continue;
					current = list.get(i);
					list.remove(i);
					break;
				}

			}else{ isRunning = false; }

			// There was not a Process Ready to be run, count as idle and carry on
			if( i > list.size() || !isRunning ){
				out.println( "Time "+ timeRun +": Idle" );
				timeRun += cBurst;
				if( timeRun > runfor ) timeRun = runfor;
				out.println( "Time "+ timeRun + ": Idle finished." );
				continue;
			}
			
			// to help readablilty/avoid reusing getter too much
			cBurst = current.getBurst();
			cArrival = current.getArrival();
			cName = current.getName();

			// first time this process has been ready state it has arrived
			if( hasArrived.add( current ) )
				out.println( "Time "+ cArrival + ": "+ cName +" arrived." ); 
			
			// state this process has been selected to run
			out.println( "Time "+ timeRun + ": "+ cName +" selected (burst "+ cBurst +")." );

			// update time run and state that it is finished
			timeRun += cBurst;

			for( Process proc : list ){
				if( proc.getArrival() > timeRun ) break;
				// first time this process has been ready state it has arrived
				if( hasArrived.add( proc ) )
					out.println( "Time "+ proc.getArrival() + ": "+ proc.getName() +" arrived." ); 
			}

			out.println( "Time "+ timeRun + ": "+ cName +" finished." );
				
			// turnaround is how long it took to get from arrival to completion
			int turn = timeRun - cArrival;

			// wait is the difference between turn and time to run
			int wait = turn - current.getFirstBurst();

			// store for easy printing
			waitround.add( new String( cName +" wait "+ wait +" turnaround "+ turn +"." ) );
		}
		// print the total time the scheduler ran  
		out.println( "Finished at Time "+ timeRun +".\n" );
		
		// Print waits and turn time
		for( i=0; i<waitround.size(); i++ ) out.println( waitround.get(i) );
	}
	
	// shortest job first
	public static void sjf(){
		//initialize variables and queues
		int programCounter = 1;
		int timer = 0;
		int i = 0;
		ArrayList<Process> readyQueue  = new ArrayList<Process>();
		ArrayList<Process> finishQueue = new ArrayList<Process>();
		
		//add the first item on list to the queue.
		readyQueue.add(list.get(0));
		out.println( "Time 0" + ": "+ list.get(0).getName() +" arrived." );
               
		//until we reach runfor OR readyQueue is empty
		for(timer = 0; timer < runfor && readyQueue.size()>0; timer++){
			//check the list for a process to add to ready queue
			while(programCounter<list.size() && timer == list.get(programCounter).getArrival()){
                            
				//if the readyQueue is not empty
				if(!(readyQueue.size()==0)){
					for(i = 0; i< readyQueue.size() && readyQueue.get(i).getBurst() < list.get(programCounter).getBurst(); i++);  //find where process belongs
				} else i=0; //Prevents Out of Bounds error

				//add to valid location of readyQueue based on burst time
				readyQueue.add(i, list.get(programCounter));
				out.println( "Time "+ timer + ": "+ list.get(programCounter).getName() +" arrived." );
				programCounter++;
                            
				//if a new item was added at beginning of queue, it is selected
				if( i == 0 && readyQueue.size()>0){
					out.println( "Time "+ timer + " : "+ readyQueue.get(0).getName() +" selected (burst "+ readyQueue.get(0).getBurst() +")." );
				}                  
			}
			
			if(!readyQueue.isEmpty()){
				
				//check to see if process is complete, if so add to finished queue and do it's paperwork for later.
				if(readyQueue.get(0).timeRan == readyQueue.get(0).getBurst()){
					readyQueue.get(0).timeIdle = timer 
							                   - readyQueue.get(0).getBurst() 
							                   - readyQueue.get(0).getArrival();
					out.println( "Time "+ timer + ": "+ readyQueue.get(0).getName() +" finished." );
					finishQueue.add(readyQueue.get(0));
					readyQueue.remove(0);
					if(readyQueue.size()>0){
						out.println("Time " + timer + " : " + readyQueue.get(0).getName() +" selected (burst "+ readyQueue.get(0).getBurst() + ").");
						readyQueue.get(0).timeRan++;
					}
				}
				//otherwise, increase the time the process continued
				else readyQueue.get(0).timeRan++;
			}
		}
		//end of processing

		out.println();
		//results of process queue, shows all processes that finished
		for(Process temp : finishQueue){
			out.println(temp.getName() + " - Wait: " + temp.timeIdle + " Turnaround: " + (temp.timeRan+temp.timeIdle));
		}
             
		out.println( "Finished at Time "+ timer +".\n" );
	}

	// round robin
	public static void rr(){
		
		// while there are processes or quantum to run through
		while( isRunning ){

			// we are at the not done get a ready process and carry on 
			if( list.size() != 0 ){
			
				// if the process is not ready to run, try the next one
				for( i=0; i<=list.size(); i++ ){
					if( list.get(i).getArrival() > timeRun ) continue;
					current = list.get(i);
					list.remove(i);
					break;
				}

			}else{ isRunning = false; }

			// There was not a Process Ready to be run, count as idle and carry on
			if( i > list.size() || !isRunning ){
				out.println( "Time "+ timeRun +": Idle" );
				timeRun += cBurst;
				if( timeRun > runfor ) timeRun = runfor;
				out.println( "Time "+ timeRun + ": Idle finished." );
				continue;
			}
			
			// to help readablilty/avoid reusing getter too much
			cBurst = current.getBurst();
			cArrival = current.getArrival();
			cName = current.getName();
			// first time this process has been ready state it has arrived
			if( hasArrived.add( current ) )
				out.println( "Time "+ cArrival + ": "+ cName +" arrived." ); 

			for( Process proc : list ){
				if( proc.getArrival() > timeRun ) break;
				// first time this process has been ready state it has arrived
				if( hasArrived.add( proc ) )
					out.println( "Time "+ proc.getArrival() + ": "+ proc.getName() +" arrived." ); 
			}

			// not the last time process needs to run, add back to the list
			if( cBurst > quantum ){

				// state this process has been selected to run
				out.println( "Time "+ timeRun + ": "+ cName +" selected (burst "+ cBurst +")." );

				// process completed some time update timeRun and burst remaining
				timeRun += quantum;
				current.setBurst( cBurst - quantum );

				// return process to the end of the list
				list.add( current );

			}else{ // this is the last time a process will run

				// state this process has been selected to run
				out.println( "Time "+ timeRun + ": "+ cName +" selected (burst "+ cBurst +")." );
				// update time run and state that it is finished
				timeRun += cBurst;
				out.println( "Time "+ timeRun + ": "+ cName +" finished." );
				
                                
				// turnaround is how long it took to get from arrival to completion
				int turn = timeRun - cArrival;

				// wait is the difference between turn and time to run
				int wait = turn - current.getFirstBurst();

				// store for easy printing
				waitround.add( new String( cName +" wait "+ wait +" turnaround "+ turn +"." ) );
			}
		}
		// print the total time the scheduler ran  
		out.println( "Finished at Time "+ timeRun +".\n" );
		
		// Print waits and turn time
		for( i=0; i<waitround.size(); i++ ) out.println( waitround.get(i) );
	}
	
	//adds the process in order based on arrival, burst, and in which it comes in during input.
	public static void addInOrder(Process newProcess){
		for(i=0; i < list.size(); i++){
			
			if(newProcess.getArrival()<list.get(i).getArrival()){
				list.add(i, newProcess);
				return;
			}
			if(newProcess.getArrival()==list.get(i).getArrival()){
					if(newProcess.getBurst()<list.get(i).getBurst()){ list.add(i, newProcess); return; }
					if(newProcess.getBurst() == list.get(i).getBurst()) {
							if(newProcess.getProcessNumber()<list.get(i).getProcessNumber()) { 
								list.add(i, newProcess);
								return;
							}
					}
			}
		}
		list.add(newProcess);
	}
}
